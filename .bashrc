# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

export TERM=xterm-256color
export PS1="\W "
# export RUST_SRC_PATH=~/.rust/src
# export PATH=$PATH:/home/denis/.cargo/bin
# export RUST_SRC_PATH=$(rustc --print sysroot)/lib/rustlib/src/rust/src/
# export PATH=$PATH:/home/denis/.cargo/bin

# export PATH=$PATH:/usr/local/go/bin
# export GOPATH=$HOME/go
# export GOBIN=$GOPATH/bin
# export PATH=$PATH:$GOPATH/bin

eval $(thefuck --alias)
# set -o vi

alias t='tree -L 1'
alias pipl='pip list --format=columns'
alias iv='vim ~/.vimrc'
alias m='cd ~/music && cmus'
alias v='/usr/bin/vi'
alias a='source virtuals/bin/activate'
alias d='deactivate'

alias serv='python manage.py runserver'
alias shell='python manage.py shell'
# alias serv='nohup python manage.py runserver > ../nohup.out  &'
alias tl='tail ../nohup.out'
# alias theme='wget -O gogh https://git.io/vKOB6 && chmod +x gogh && ./gogh'

# alias serv='nohup python manage.py runserver >/dev/null 2>&1 &'
# alias gogo='cd ~/go/src/bitbucket.org/dkuanyshbaev'
# alias gogo='go run ./main.go'
alias count='ls | wc -l'
# alias lo='slock'

alias gotomaya='ssh denis@185.4.64.103'
