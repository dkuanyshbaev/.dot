;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; repo
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;'("melpa" . "http://melpa.org/packages/") t)


(require 'package)
(add-to-list 'package-archives 
	     '("melpa-stable" . "https://stable.melpa.org/packages/"))
(package-initialize)
(package-refresh-contents)


;; ?
;(unless (package-installed-p 'cider)
;  (package-install 'cider))

;; cider
;; rainbow-delimiters
;; paredit
;; clojur-mode
;; magit
;; zenburn-theme
;; auto-compl
;; projectile
;; neotree ?
;; smex ?


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; look&feel
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;(load-theme 'zenburn t)
(set-default-font "roboto mono 14")

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(show-paren-mode 2)

(set-default 'truncate-lines t)
(setq inhibit-startup-message t)
(setq ring-bell-function 'ignore)

;; auto-shmauto
(setq make-backup-files         nil)
(setq auto-save-list-file-name  nil)
(setq auto-save-default         nil)

;; linum
;; (global-linum-mode)
;; (setq linum-format "%d ")

;; ido
(ido-mode t)
(setq ido-enable-flex-matching t)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; keys
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;(global-set-key (kbd "C-f") 'neotree-toggle)

;; buff
(global-set-key (kbd "C-,") 'bs-show)
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; move
(global-set-key (kbd "M-l") 'right-char)
(global-set-key (kbd "M-h") 'left-char)
(global-set-key (kbd "M-k") 'previous-line)
(global-set-key (kbd "M-j") 'next-line)

;; comments
(global-set-key (kbd "C-c C-c") 'comment-region)
(global-set-key (kbd "C-c C-u") 'uncomment-region)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; outer
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; powerline
;(powerline-center-theme)
;(powerline-default-theme)
;(powerline-nano-theme)

;; auto-compl
;(ac-config-default)

;; magit
;(global-set-key (kbd "C-|") 'magit-status)

;; rainbows
;(global-rainbow-delimiters-mode t)

;; paredit
;(autoload 'enable-paredit-mode "paredit" "The editing begins!" t)

;; projectile
;(projectile-global-mode)

;; smex
;; (global-set-key (kbd "M-x") 'smex)

;; clojure
;; (add-to-list 'same-window-buffer-names "<em>nrepl</em>")
;; (setq nrepl-popup-stacktraces nil)
;; (setq nrepl-hide-special-buffers t)

;; (add-hook 'clojure-mode-hook 'enable-paredit-mode)
;; (add-hook 'clojure-mode-hook 'turn-on-eldoc-mode)
;; (add-hook 'cider-repl-mode-hook #'paredit-mode)
;; (add-hook 'cider-repl-mode-hook #'rainbow-delimiters-mode)

;; ;; lisps
;; (add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
;; (add-hook 'scheme-mode-hook           #'enable-paredit-mode)
;; (add-hook 'lisp-mode-hook             #'enable-paredit-mode)
;; (add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; notes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; org-mode
;; tab - open
;; S-tab - open all
;; M-return - create new subelement
;; S-M-return - create new TODO
;; M-left, M-right - deep
;; S-M-left, S-M-right - deep with subitems
;; S-M-up, S-M-down - move tree
;; C-c C-c - check list
;; C-c C-z - time stamped drawer
;; C-c C-x f - footnote-mode
;; C-c C-t - todo state
;; S-left, S-right - todo state
;; C-c C-e - export

;; ediff
;; emacs -nw
;; emacs --daemon (emacsclient -c filename)

;; marks
;; set: C-x r m
;; list: C-x r l
;; jump: C-x r b
